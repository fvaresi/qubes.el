;;; qubes.el --- Better Emacs Integration for Qubes OS
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2017, 2018 Jens Lechtenbörger

;; Compatibility: GNU Emacs 26.x

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.
;; If not, see http://www.gnu.org/licenses/ or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;; Keywords: Qubes, disposable virtual machine, conversion, trusted PDF

;;; Commentary:

;; Place this file into your load-path and add the following to ~/.emacs.
;; ;;; Qubes integration
;; ;; Setup Split GPG
;; ;; (require 'epg-config)
;; (customize-set-variable 'epg-gpg-program "/usr/bin/qubes-gpg-client-wrapper")
;; ;; (push (cons 'OpenPGP (epg-config--make-gpg-configuration epg-gpg-program))
;; ;;       epg--configurations)

;; (require 'qubes)
;; (setq browse-url-browser-function 'qubes-browse)

;; ;; Also allow to open PDF files in Disposable VMs.
;; ;; Add the following line to ~/.mailcap:
;; ;; application/*; qvm-open-in-dvm %s
;; (require 'mailcap)
;; (mailcap-parse-mailcaps)

;; ;; Define key binding to work on files in VMs.
;; (add-hook 'dired-mode-hook
;; 	  (lambda ()
;; 	    (define-key dired-mode-map "ö" 'jl-dired-copy-to-qvm)
;; 	    (define-key dired-mode-map "ä" 'jl-dired-open-in-dvm)
;; 	    (define-key dired-mode-map "ü" 'jl-dired-qvm-convert)
;; 	    ))

;; (add-hook 'gnus-article-mode-hook
;; 	  (lambda ()
;; 	    (define-key gnus-article-mode-map
;; 	      "ä" 'jl-gnus-article-view-part-in-dvm)
;; 	    (define-key gnus-summary-mode-map
;; 	      "ä" 'jl-gnus-article-view-part-in-dvm)
;;	    (define-key gnus-mime-button-map
;;	      "ö" 'jl-gnus-mime-copy-to-vm)
;; 	    (define-key gnus-mime-button-map
;; 	      "ü" 'jl-gnus-mime-qvm-convert-and-display)
;; 	    (define-key gnus-article-mode-map
;; 	      "ü" 'jl-gnus-article-view-trusted-part-via-qubes)
;; 	    (define-key gnus-summary-mode-map
;; 	      "ü" 'jl-gnus-article-view-trusted-part-via-qubes)
;; 	    ))

;; This file is *NOT* part of GNU Emacs.

;;; Code:

;; Customizable variables start here.

(defgroup qubes nil
  "Customization options for Qubes OS."
  :version "26.1"
  :group 'external)

(defcustom qubes-vms '((?a "anon-whonix" "VM with torified network traffic")
		       (?c "current" "The VM in which Emacs is running")
		       (?d "disp-vm" "Disposable VM, reverting changes")
		       (?w "work" "VM with ordinary settings"))
  "Qubes VMs that may be used when browsing URLs.
This variable is a list whose entries are three-element lists consisting
of a character, a VM name and a description for that VM.  The character is
used for user selections in `qubes-browse'."
  :group 'qubes
  :type '(repeat (list character string string)))

(defcustom qubes-browser "firefox -P default"
  "Browser command to be used in the VM running Emacs."
  :group 'qubes
  :type 'string)

(defcustom qubes-url-vm-mapping
  '(("^https?:.*[.]uni-muenster[.]de.*" . "work")
    ("^https:.*[.]sciebo[.]de.*" . "work")
    ("^file:.*" . "current"))
  "Mapping of regular expressions matching URLs to VMs.
This variable is a list of cons cells.  Each cons cell is of the form
\(regexp . vm).  In `qubes-vm-for-url', if the regexp matches a given URL
the corresponding VM is returned.
The VM string \"current\" has a special meaning and denotes the VM in
which Emacs is running."
  :group 'qubes
  :type '(repeat (cons regexp string)))

(defcustom qubes-qvm-untrusted-image-dir "~/QubesUntrustedIMGs"
  "Directory in which to store untrusted image files.
Set to nil if you want to keep images where they are saved."
  :group 'qubes
  :type 'directory)

;; No customizable variables beyond this point.

(defvar qubes-open-in-dvm "qvm-open-in-dvm \"%s\" &"
  "Qubes command to open file in disposable machine.")

(defvar qubes-open-in-vm "qvm-open-in-vm %s \"%s\" &"
  "Qubes command to open file in virtual machine.")

(defvar qubes-copy-to-vm "qvm-copy-to-vm %s \"%s\""
  "Qubes command to copy file to virtual machine.")

(defvar qubes-repair-url-options '((?s "https://" "Prepend https:// to URL")
				   (?h "http://" "Preprend http:// to URL")
				   (?m "manually" "Fix URL manually")
				   (?a "abort" "Abort"))
  "Options how to repair a URL that is no URL.")

(defvar qubes-url-regexp "^\\(https?\\|file\\)://"
  "Regular expression to identify valid URLs.")

(defun qubes-repair-url (url)
  "Repair URL that does not match `qubes-url-regexp'."
  (if (string-match qubes-url-regexp url)
      url
    (let ((choice (cadr (read-multiple-choice
			 "Invalid URL.  Fix how?" qubes-repair-url-options))))
      (cond ((string= choice "abort") (error "Invalid URL: %s" url))
	    ((string= choice "manually")
	     (read-from-minibuffer "What URL (maybe use next history item)? "
				   nil nil nil nil url))
	    (t (concat choice url))))))

(defun qubes-vm-for-url (url)
  "Return virtual machine in which to open URL.
Uses `qubes-url-vm-mapping' for automatic decisions.  If no match is found
via `qubes-url-vm-mapping', ask user with options determined by `qubes-vms'."
  (catch 'break
    (dolist (pair qubes-url-vm-mapping nil)
      (when (string-match (car pair) url)
	(throw 'break (cdr pair))))))

(defun qubes-browse (url &rest args)
  "Browse URL, ignoring ARGS.
First check that URL really is a URL.
Uses `qubes-vm-for-url' to find a VM automatically.  If no match is found
via that function, ask the user with options determined by `qubes-vms'."
  (let* (;; (url (qubes-repair-url url))
	 (vm (or
	      (qubes-vm-for-url url)
              (ido-completing-read "Which VM? " qubes-vms)
	      (cadr (read-multiple-choice "Which VM?" qubes-vms))))
	 (qurl (shell-quote-argument url))
	 (command
	  (cond ((string= vm "current") (format "%s %s &" qubes-browser qurl))
		((string= vm "disp-vm")
		 (format qubes-open-in-dvm qurl))
		(t (format qubes-open-in-vm vm url)))))
    (shell-command command)))

(defun qubes-qvm-convert (filename)
  "Convert FILENAME to trusted format via Qubes disposable VM.
Return name of the converted file.
Untrusted PDFs are moved to \"~/QubesUntrustedPDFs\" by the
conversion command.  Untrusted images are moved if
`qubes-qvm-untrusted-image-dir' is non-nil."
  (interactive)
  (let ((ext (file-name-extension filename)))
    (if ext
	(let* ((sans (file-name-sans-extension filename))
	       (trusted (format "%s.trusted.%s" sans ext))
	       (case-fold-search t)
	       (command (cond ((string= "pdf" ext)
			       (format "qvm-convert-pdf %s"
				       (shell-quote-argument filename)))
			      ((string-match "png\\|jpg\\|gif" ext)
			       (format "qvm-convert-img %s %s"
				       (shell-quote-argument filename)
				       (shell-quote-argument trusted)))
			      (t (error "Unknown filename extension %s" ext)))))
	  (shell-command command)
	  (when (and qubes-qvm-untrusted-image-dir
		     (not (string= "pdf" ext)))
	    (make-directory qubes-qvm-untrusted-image-dir t)
	    (rename-file filename qubes-qvm-untrusted-image-dir 1))
	  trusted)
      (error "File without extension.  How to convert?  %s" filename))))

;; Dired functionality

(defun jl-dired-qvm-convert ()
  "Convert the current line's file to trusted format via Qubes."
  (interactive)
  (qubes-qvm-convert (dired-get-file-for-visit)))

(defun jl-dired-open-in-dvm ()
  "Open the current line's file in a disposable VM."
  (interactive)
  (shell-command (format qubes-open-in-dvm
			 (shell-quote-argument (dired-get-file-for-visit)))))

(defun jl-dired-copy-to-qvm (machine)
  "Copy selected files to Qubes MACHINE."
  (interactive "sCopy to VM: ")
  (let ((in-files (dired-get-marked-files)))
    (dolist (file in-files)
      (shell-command
       (format qubes-copy-to-vm machine (shell-quote-argument file))))))

;; Gnus functionality

(defun jl-gnus-mime-qvm-convert ()
  "Convert PDF or image to trusted format via Qubes disposable VM.
Save an untrusted MIME part, run conversion command on that file, and
return name of the converted file.
Untrusted PDFs are moved to \"~/QubesUntrustedPDFs\" by the
conversion command.  Untrusted images are moved if
`qubes-qvm-untrusted-image-dir' is non-nil."
  (interactive)
  (qubes-qvm-convert (gnus-mime-save-part)))

(defun jl-gnus-mime-copy-to-vm (machine)
  "Save attachment and copy to Qubes MACHINE."
  (interactive "sCopy to VM: ")
  (shell-command
   (format qubes-copy-to-vm machine (gnus-mime-save-part))))

(defun jl-gnus-mime-view-part-in-dvm ()
  "View MIME part under point in Qubes disposable VM."
  (interactive)
  (gnus-article-check-buffer)
  (let ((data (get-text-property (point) 'gnus-data)))
    (when data
      (mm-display-external data qubes-open-in-dvm))))

(defun jl-gnus-mime-qvm-convert-and-display ()
  "Convert PDF or image to trusted format via Qubes and display it.
See `jl-gnus-mime-qvm-convert' for more information."
  (interactive)
  (find-file-other-frame (jl-gnus-mime-qvm-convert)))

(defun jl-gnus-article-view-trusted-part-via-qubes (n)
  "Convert part N via Qubes into trusted format and view that.
Conversion only works for PDFs and images.
See `jl-gnus-mime-qvm-convert' for more information.
N is the numerical prefix."
  (interactive "P")
  (gnus-article-part-wrapper n 'jl-gnus-mime-qvm-convert-and-display t))

(defun jl-gnus-article-view-part-in-dvm (n)
  "View part N in Qubes disposable VM.
N is the numerical prefix."
  (interactive "P")
  (gnus-article-part-wrapper n 'jl-gnus-mime-view-part-in-dvm t))

(provide 'qubes)
;;; qubes.el ends here
